# CONCEPT: C8B2 (MAKER)
Status: Design

---

## Team Lead(s):
|Name|GitLab|
|---|---|
| Colin O'Brien |  |
| Dave Graham | |

## Concept Overview:

### Introduction

### Function

### Mechanical Design

### Electrical

### Manufacturability

### Aesthetic

### Known Problems / Considerations

## Issue Labels:
~"CXYZ"

## Related Issues and Boards:
- Feedback Thread #0

## Other Important Links
