# P1A1 Controller Housing - Overview
Mark A Lonsdale Aug 7, 2020 r1.0

---

The P1A1 Controller Housing [Release 1] can be broken down into 5 specific assemblies.

1) The main [common] housing assembly with contains the sensor cartridge, the electronics tray, and a removable rear panel.
The main housing is designed to  support a custom front panel that can be designed to accommodate the mechanical footprint of the ventilator and the orientation/HMI requirements of the specific design.

2) The Rear panel is removable to support customization of the power, motor and airflow needs of the specific ventilator controller design.

3) The electronics tray houses the MCU, power management, discrete electronics and low power stepper driver modules.
The default design supports maker modules such as an Arduino Nano MCU mounted on a terminal shield, popular DC Step-down power modules and low power stepper driver modules.
 * Note. The main housing assembly can accommodate an industrial style stand alone high current stepper driver module in the main housing, adjacent (but external to) the electronics tray.

4) The Sensor cartridge allows for a syringe mounted sensor to be inserted and removed from the main housing assembly, independently of the controller electronics.  This allow for easy replacement/maintenance of the sensor and the ability to interchange the sensor type without complex assembly in the field.

5) The custom front panel which can be attached to the main housing but be designed to meet the specific HMI design and orientation requirements of the different mechanical designs.
---

## Design Notes / Open Issues