/**
 * alarm.h
 *
 * Author: Corbin Bremmeyr
 * Date: 8 April 2020
 *
 * Functions to interact with alarm board that is part of the OSV
 */

#ifndef ALARM_DRIVER_H
#define ALARM_DRIVER_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Warning levels for the alarm board
 */
typedef enum {
    no_warning,
    low_warning,
    med_warning,
    high_warning
} alarm_warn_t;

/**
 * Initialize the alarm board library and interface
 */
void alarm_init(void);

/**
 * Trigger emergency alarm
 *
 * param enable - `true` to activate alarm, `false` to deactivate
 */
void alarm_emergency_now(bool enable);

/**
 * Arm or disarm the emergency alarm
 *
 * param arm - `true` to arm the alarm, `false` to disarm
 */
void alarm_set_monitor_stat(bool arm);

/**
 * Set the warning level of the alarm board
 *
 * param warn_level - warning level to set
 */
void alarm_set_warn(alarm_warn_t warn_level);

/**
 * Control if the audiable component of the warning alarm is muted
 *
 * param mute - `true` to mute warning alarm, `false` to unmute
 */
void alarm_mute_warn(bool mute);

/**
 *  Sends a pulse to the alarm board to reset the watchdog timer
 */
void alarm_kick_watchdog(void);

#ifdef __cplusplus
}
#endif

#endif

