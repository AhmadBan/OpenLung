/**
 * alarm.ino
 *
 * Author: Corbin Bremmeyr
 * Date: 8 April 2020
 *
 * Functions to interact with alarm board
 */

// TODO list:
//       - functions to set warning alarm volume
//          - dependant on hardware selection
//          - also add to alarm_init() if needed
//       - other TODO's

#include <Arduino.h>
#include "alarm.h"

// Pin assignments
// TODO: add correct values once pin assignment is finialized
#define WARNING_ALARM_0     (1)
#define WARNING_ALARM_1     (1)
#define WARNING_MUTE        (1)
#define SYSTEM_OK           (1)
#define EMERGENCY_NOW       (1)
#define MONITOR_STATUS      (1)
#define MONITOR_STATUS_CLK  (1)

// Pulse width for SYSTEM_OK when watchdog is reset (units: us)
// TODO: add correct value when timing limits of alarm board are known
#define SYSTEM_OK_PULSE (1)

// Timing values for clocking monitor_status bit to alarm board (units: us)
// TODO: add correct values when timing limits of alarm board are known
#define MONITOR_SETUP    (1)
#define MONITOR_CLK_HIGH (1)

/**
 * Initialize the alarm board library and interface. The monitor status defualts to disarmed
 */
void alarm_init(void) {

    // Set all pins to LOW
    digitalWrite(WARNING_ALARM_0, LOW);
    digitalWrite(WARNING_ALARM_1, LOW);
    digitalWrite(WARNING_MUTE, LOW);
    digitalWrite(SYSTEM_OK, LOW);
    digitalWrite(EMERGENCY_NOW, LOW);
    digitalWrite(MONITOR_STATUS, LOW);
    digitalWrite(MONITOR_STATUS_CLK, LOW);

    // Set all pins as OUTPUTS
    pinMode(WARNING_ALARM_0, OUTPUT);
    pinMode(WARNING_ALARM_1, OUTPUT);
    pinMode(WARNING_MUTE, OUTPUT);
    pinMode(SYSTEM_OK, OUTPUT);
    pinMode(EMERGENCY_NOW, OUTPUT);
    pinMode(MONITOR_STATUS, OUTPUT);
    pinMode(MONITOR_STATUS_CLK, OUTPUT);

    // Disarm alarms
    alarm_set_monitor_stat(false);
}

/**
 * Trigger emergency alarm
 *
 * param enable - `true` to activate alarm, `false` to deactivate
 */
void alarm_emergency_now(bool enable) {
        digitalWrite(EMERGENCY_NOW, enable);
}

/**
 * Arm or disarm the emergency alarm
 *
 * param arm - `true` to arm the alarm, `false` to disarm
 */
void alarm_set_monitor_stat(bool arm) {
    digitalWrite(MONITOR_STATUS_CLK, LOW);

    // Set status value to send
    digitalWrite(MONITOR_STATUS, arm);
    delayMicroseconds(MONITOR_SETUP);

    // Pulse clk signal
    digitalWrite(MONITOR_STATUS_CLK, HIGH);
    delayMicroseconds(MONITOR_CLK_HIGH);
    digitalWrite(MONITOR_STATUS_CLK, LOW);
}

/**
 * Set the warning level of the alarm board
 *
 * param warn_level - warning level to set
 */
void alarm_set_warn(alarm_warn_t warn_level) {

    switch(warn_level) {

        case no_warning:
            digitalWrite(WARNING_ALARM_0, LOW);
            digitalWrite(WARNING_ALARM_1, LOW);
            break;

        case low_warning:
            digitalWrite(WARNING_ALARM_0, LOW);
            digitalWrite(WARNING_ALARM_1, HIGH);
            break;

        case med_warning:
            digitalWrite(WARNING_ALARM_0, HIGH);
            digitalWrite(WARNING_ALARM_1, LOW);
            break;

        case high_warning:
            digitalWrite(WARNING_ALARM_0, HIGH);
            digitalWrite(WARNING_ALARM_1, HIGH);
            break;

        // Currently triggers no alarm, maybe this should trigger some alarm?
        default:
            digitalWrite(WARNING_ALARM_0, LOW);
            digitalWrite(WARNING_ALARM_1, LOW);
            break;
    }
}

/**
 * Control if the audiable component of the warning alarm is muted
 *
 * param mute - `true` to mute warning alarm, `false` to unmute
 */
void alarm_mute_warn(bool mute) {
    digitalWrite(WARNING_MUTE, mute);
}

/**
 *  Sends a pulse to the alarm board to reset the watchdog timer
 */
void alarm_kick_watchdog(void) {
    digitalWrite(SYSTEM_OK, HIGH);
    delayMicroseconds(SYSTEM_OK_PULSE);
    digitalWrite(SYSTEM_OK, LOW);
}

