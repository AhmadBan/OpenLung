# CONCEPT:
---
## Scope:
- Hardware related to the humidification and temperature maintenance of ventilated air, not including sensors and ventilator itself

## Team Lead(s):
|GitLab|Slack|
|---|---|
| | @Olivier Coispeau |
| | Peter Knief|
| | @TrevorSmale

## Slack Channels:
-

## Issue Labels:
- Humidifier Hardware

## Overview

The focus of this project area is to create a humidification device that can be placed inline with the patient ventilation circuit.
The idea is to create open plans for the construction of a small module made from simple, easily sourced materials that can operate for long periods of time with few adjustment or maintenance.

## Methods

Two humification methods have been proposed, one operating electromechanically by heat evaporation and one operating on the mechanical properties of wicking and temperature difference.

### Heat evaporation method

By placing a piezo heating element beneath a container of water, a controlled evaporation of heated water can be achieved. Capturing and delineating steam for addition to the circuit happens by way of a manifold covering the liquid container. This method is typical in most ventilators.

### Wicking method

By placing an object with large amounts of porosity and surface area in contact with a liquid container, the wicking action will transport water from containment to dispersal through regular evaporation.
